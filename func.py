#!/usr/bin/python3

class Number:
    def __init__(self, n):
        self.n = n

    def accept(self, vis):
        return vis.visitNumber(self)

    def __add__(self, other):
        if isinstance(other, Number):
            return Number(self.n + other.n)
        else:
            return Number(self.n + other)

    def __radd__(self, other):
        return self + other

    def __str__(self):
        return str(self.n)


class Poly:
    def __init__(self, coef):
        self.coef = coef[:]

    def accept(self, vis):
        return vis.visitPoly(self)

    def __add__(self, other):
        if isinstance(other, Poly):
            return Poly([(self.coef[i] if i < len(self.coef) else 0) + (other.coef[i] if i < len(other.coef) else 0) for i in range(max(len(self.coef), len(other.coef)))])
        else:
            p = self.coef[:]
            p[0] += other
            return Poly(p)

    def __radd__(self, other):
        return self + other
            


default_class = """
class %fnname%:
    def __init__(self, inner):
        self.inner = inner

    def accept(self, vis):
        return vis.visit%fnname%(self)
"""

exec(default_class.replace("%fnname%", "Exp"))
exec(default_class.replace("%fnname%", "Log"))
exec(default_class.replace("%fnname%", "Sin"))
exec(default_class.replace("%fnname%", "Cos"))
exec(default_class.replace("%fnname%", "Tg"))
exec(default_class.replace("%fnname%", "Ctg"))
exec(default_class.replace("%fnname%", "Arctg"))
exec(default_class.replace("%fnname%", "Arcsin"))
exec(default_class.replace("%fnname%", "Divx"))
exec(default_class.replace("%fnname%", "Sqrt"))

default_binary_operation = """
class %opname%:
    def __init__(self, first, second):
        self.first = first
        self.second = second

    def accept(self, vis):
        return vis.visit%opname%(self)
"""

exec(default_binary_operation.replace("%opname%", "Sum"))
exec(default_binary_operation.replace("%opname%", "Product"))
exec(default_binary_operation.replace("%opname%", "Quotient"))

default_ext_operation = """
class %opname%:
    def __init__(self, lst):
        self.lst = lst

    def accept(self, vis):
        return vis.visit%opname%(self)
"""

exec(default_ext_operation.replace("%opname%", "ExtSum"))
exec(default_ext_operation.replace("%opname%", "ExtProduct"))
