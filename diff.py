#!/usr/bin/python3

from func import *

class Diff:
    def __init__(self, inner):
        self.inner = inner

    def accept(self, vis):
        return vis.visitDiff(self)


class DiffVisitor:
    def __init__(self):
        pass

    def visit(self, fn):
        return fn.accept(self)

    def visitNumber(self, fn):
        return Number(0)

    def visitPoly(self, fn):
        coef = fn.coef[1:]
        for i in range(len(coef)):
            coef[i] *= (i + 1)
        return Poly(coef)

    def visitExp(self, fn):
        return Product(Exp(fn.inner), self.visit(fn.inner))

    def visitLog(self, fn):
        return Quotient(self.visit(fn.inner), fn.inner)

    def visitSin(self, fn):
        return Product(Cos(fn.inner), self.visit(fn.inner))

    def visitCos(self, fn):
        return Product(Number(-1), Product(Sin(fn.inner), self.visit(fn.inner)))

    def visitTg(self, fn):
        return Product(Sum(Product(Tg(fn.inner), Tg(fn.inner)), Number(1)), self.visit(fn.inner))
    
    def visitCtg(self, fn):
        return Quotient(Product(Number(-1), self.visit(fn.inner)), Sin(fn.inner) * Sin(fn.inner))

    def visitArctg(self, fn):
        return Quotient(self.visit(fn.inner), Sum(Number(1), Product(fn.inner, fn.inner)))

    def visitArcsin(self, fn):
        return Quotient(self.visit(fn.inner), Sqrt(Sum(Number(1), Product(Number(-1), Product(fn.inner, fn.inner)))))

    def visitDivx(self, fn):
        return Quotient(Product(Number(-1), self.visit(fn.inner)), Product(fn.inner, fn.inner))

    def visitSqrt(self, fn):
        return Qutient(self.visit(fn.inner), Product(Number(2), Sqrt(fn.inner)))

    def visitSum(self, s):
        return Sum(self.visit(s.first), self.visit(s.second))

    def visitProduct(self, p):
        return Sum(Product(self.visit(p.first), p.second), Product(self.visit(p.second), p.first))

    def visitQuotient(self, p):
        return Quotient(Sum(Product(p.first, self.visit(p.second)), Product(Number(-1), Product(p.second. self.visit(p.first)))), Product(p.second, p.second))

    def visitExtSum(self, p):
        return ExtSum(list(map(lambda x: self.visit(x), p.lst)))

    def visitExtProduct(self, p):
        if len(p.lst) == 1:
            return self.visit(p.lst[0])
        else:
            return self.visit(Product(p.lst[0], ExtProduct(p.lst[1:])))


class TeXPrinter:
    def __init__(self):
        self.s = ""

    def visit(self, p):
        p.accept(self)

    def visitNumber(self, p):
        if p.n < 0:
            self.s += "("
        self.s += str(p.n)
        if p.n < 0:
            self.s += ")"

    def visitPoly(self, p):
        arr = []
        for x in range(len(p.coef) - 1, -1, -1):
            if p.coef[x] != 0:
                s = ""
                if x == 0 or p.coef[x] != 1:
                    s = str(p.coef[x])
                if x > 0:
                    s += "x"
                if x > 1:
                    s += "^{" + str(x) + "}"
                arr.append(s)
        self.s += " + ".join(arr)
    
    def visitExp(self, p):
        self.s += "\\exp "
        if isinstance(p.inner, Sum) or isinstance(p.inner, ExtSum):
            self.s += "\\left("
        self.visit(p.inner)
        if isinstance(p.inner, Sum) or isinstance(p.inner, ExtSum):
            self.s += "\\right)"

    def visitLog(self, p):
        self.s += "\\ln "
        if isinstance(p.inner, Sum) or isinstance(p.inner, ExtSum):
            self.s += "\\left("
        self.visit(p.inner) 
        if isinstance(p.inner, Sum) or isinstance(p.inner, ExtSum):
            self.s += "\\right)"

    def visitSin(self, p):
        self.s += "\\sin " 
        if isinstance(p.inner, Sum) or isinstance(p.inner, ExtSum):
            self.s += "\\left("
        self.visit(p.inner) 
        if isinstance(p.inner, Sum) or isinstance(p.inner, ExtSum):
            self.s += "\\right)"

    def visitCos(self, p):
        self.s += "\\cos " 
        if isinstance(p.inner, Sum) or isinstance(p.inner, ExtSum):
            self.s += "\\left("
        self.visit(p.inner) 
        if isinstance(p.inner, Sum) or isinstance(p.inner, ExtSum):
            self.s += "\\right)"

    def visitTg(self, p):
        self.s += "\\tg " 
        if isinstance(p.inner, Sum) or isinstance(p.inner, ExtSum):
            self.s += "\\left("
        self.visit(p.inner) 
        if isinstance(p.inner, Sum) or isinstance(p.inner, ExtSum):
            self.s += "\\right)"

    def visitCtg(self, p):
        self.s += "\\ctg " 
        if isinstance(p.inner, Sum) or isinstance(p.inner, ExtSum):
            self.s += "\\left("
        self.visit(p.inner)
        if isinstance(p.inner, Sum) or isinstance(p.inner, ExtSum):
            self.s += "\\right)"

    def visitArctg(self, p):
        self.s += "\\arctg "
        if isinstance(p.inner, Sum) or isinstance(p.inner, ExtSum):
            self.s += "\\left("
        self.visit(p.inner)
        if isinstance(p.inner, Sum) or isinstance(p.inner, ExtSum):
            self.s += "\\right)"

    def visitArcsin(self, p):
        self.s += "\\arcsin "
        if isinstance(p.inner, Sum) or isinstance(p.inner, ExtSum):
            self.s += "\\left("
        self.visit(p.inner)
        if isinstance(p.inner, Sum) or isinstance(p.inner, ExtSum):
            self.s += "\\right)"

    def visitDivx(self, p):
        self.s += "\\frac{1}{"
        self.visit(p.inner)
        self.s += "}"

    def visitSqrt(self, p):
        self.s += "\sqrt{"
        self.visit(p.inner)
        self.s += "}"

    def visitSum(self, p):
        self.s += "\\left("
        self.visit(p.first)
        self.s += " + "
        self.visit(p.second)
        self.s += "\\right)"

    def visitProduct(self, p):
        self.s += "\\left("
        self.visit(p.first)
        self.s += " \cdot "
        self.visit(p.second)
        self.s += "\\right)"

    def visitQuotient(self, p):
        self.s += "\\frac{"
        self.visit(p.first)
        self.s += "}{"
        self.visit(p.second)
        self.s += "}"

    def visitExtSum(self, p):
        arr = []
        for t in p.lst:
            d = TeXPrinter()
            d.visit(t)
            arr.append(d.s)
        self.s += " + ".join(arr)

    def visitExtProduct(self, p):
        arr = []
        for t in p.lst:
            d = TeXPrinter()
            d.visit(t)
            if isinstance(t, Sum) or isinstance(t, ExtSum):
                arr.append("\\left(" + d.s + "\\right)")
            else:
                arr.append(d.s)
        self.s += "\\cdot".join(arr)

    def visitDiff(self, p):
        self.s += "\\left("
        self.visit(p.inner)
        self.s += "\\right)'"


def to_string(fn):
    d = TeXPrinter()
    d.visit(fn)
    return d.s


class Simplifier:
    def __init__(self):
        pass

    def visit(self, p):
        return p.accept(self)

    def visitNumber(self, p):
        return p

    def visitPoly(self, p):
        while len(p.coef) > 1 and p.coef[-1] == 0:
            p.coef.pop()
        if len(p.coef) == 1:
            return Number(p.coef[0])
        return p

    def visitExp(self, p):
        return Exp(self.visit(p.inner))

    def visitLog(self, p):
        return Log(self.visit(p.inner))

    def visitSin(self, p):
        return Sin(self.visit(p.inner))

    def visitCos(self, p):
        return Cos(self.visit(p.inner))

    def visitTg(self, p):
        return Tg(self.visit(p.inner))

    def visitCtg(self, p):
        return Ctg(self.visit(p.inner))

    def visitArctg(self, p):
        return Arctg(self.visit(p.inner))

    def visitArcsin(self, p):
        return Arcsin(self.visit(p.inner))

    def visitDivx(self, p):
        return Divx(self.visit(p.inner))

    def visitSqrt(self, p):
        return Sqrt(self.visit(p.inner))

    def visitSum(self, p):
        return self.visit(ExtSum([p.first, p.second]))

    def visitProduct(self, p):
        return self.visit(ExtProduct([p.first, p.second]))

    def visitQuotient(self, p):
        return Quotient(self.visit(p.first), self.visit(p.second))

    def visitExtSum(self, p):
        if len(p.lst) == 1:
            return self.visit(p.lst[0])
        w = []
        for t in p.lst:
            r = self.visit(t)
            if isinstance(r, ExtSum):
                w += r.lst
            else:
                if isinstance(self.visit(r), ExtSum):
                    print(to_string(t))
                    print(to_string(r))
                w.append(r)
        u = []
        v = []
        for t in w:
            r = self.visit(t)
            assert not isinstance(r, ExtSum)
            if isinstance(r, Number) or isinstance(r, Poly):
                u.append(r)
            else:
                v.append(r)
        if u == []:
            t = Number(0)
        else:
            t = self.visit(sum(u))
        if isinstance(t, Number) and t.n == 0:
            if u == []:
                return ExtSum(v)
            else:
                return self.visit(ExtProduct(v))
        else:
            return ExtSum(v + [t])


    def visitExtProduct(self, p):
        if len(p.lst) == 1:
            return self.visit(p.lst[0])
        w = []
        for t in p.lst:
            r = self.visit(t)
            if isinstance(r, ExtProduct):
                w += r.lst
            else:
                w.append(r)
        u = []
        v = []
        for t in p.lst:
            r = self.visit(t)
            if isinstance(r, Number):
                u.append(r)
            else:
                v.append(r)
        if u == []:
            t = Number(1)
        else:
            t = self.visit(sum(u))
        if isinstance(t, Number) and t.n == 0:
            return Number(0)
        elif isinstance(t, Number) and t.n == 1:
            if u == []:
                return ExtProduct(v)
            else:
                return self.visit(ExtProduct(v))
        else:
            return ExtProduct(v + [t])

    def visitDiff(self, p):
        r = self.visit(p.inner)
        if isinstance(r, Number):
            return Number(0)
        elif isinstance(r, Poly) and len(r.coef) == 2:
            return Number(r.coef[1])
        else:
            return Diff(r)


def simplify(t):
    return Simplifier().visit(t)


do_step = """
class DoStep:
    def __init__(self):
        self.inside = False
        self.finish = False

    def visit(self, p):
        return p.accept(self)

    def visitDiff(self, p):
        self.inside = True
        ans = self.visit(p.inner)
        self.inside = False
        return ans

"""
do_step_default_visit_fn = """
    def visit%fnname%(self, p):
        if not self.inside:
            return %fnname%(self.visit(p.inner))
        elif not self.finish:
            self.finish = True
            ans = DiffVisitor.visit%fnname%(self, p)
            self.finish = False
            return ans
        else:
            return Diff(p)

"""
do_step += do_step_default_visit_fn.replace("%fnname%", "Number").replace("Number(self.visit(p.inner))", "p")
do_step += do_step_default_visit_fn.replace("%fnname%", "Poly").replace("Poly(self.visit(p.inner))", "p")
do_step += do_step_default_visit_fn.replace("%fnname%", "Exp")
do_step += do_step_default_visit_fn.replace("%fnname%", "Log")
do_step += do_step_default_visit_fn.replace("%fnname%", "Sin")
do_step += do_step_default_visit_fn.replace("%fnname%", "Cos")
do_step += do_step_default_visit_fn.replace("%fnname%", "Tg")
do_step += do_step_default_visit_fn.replace("%fnname%", "Ctg")
do_step += do_step_default_visit_fn.replace("%fnname%", "Arctg")
do_step += do_step_default_visit_fn.replace("%fnname%", "Arcsin")
do_step += do_step_default_visit_fn.replace("%fnname%", "Divx")
do_step += do_step_default_visit_fn.replace("%fnname%", "Sqrt")
do_step_default_visit_op = """
    def visit%fnname%(self, p):
        if not self.inside:
            return %fnname%(self.visit(p.first), self.visit(p.second))
        elif not self.finish:
            self.finish = True
            ans = DiffVisitor.visit%fnname%(self, p)
            self.finish = False
            return ans
        else:
            return Diff(p)

"""
do_step += do_step_default_visit_op.replace("%fnname%", "Sum")
do_step += do_step_default_visit_op.replace("%fnname%", "Product")
do_step += do_step_default_visit_op.replace("%fnname%", "Quotient")
do_step_default_visit_extop = """
    def visit%fnname%(self, p):
        if not self.inside:
            return %fnname%(list(map(lambda x: self.visit(x), p.lst)))
        elif not self.finish:
            self.finish = True
            ans = DiffVisitor.visit%fnname%(self, p)
            self.finish = False
            return ans
        else:
            return Diff(p)

"""
do_step += do_step_default_visit_extop.replace("%fnname%", "ExtSum")
do_step += do_step_default_visit_extop.replace("%fnname%", "ExtProduct")

exec(do_step)


def solution(p):
    p = simplify(p)
    ans = [to_string(p)]
    while len(ans) < 2 or ans[-1] != ans[-2]:
        s = DoStep()
        p = simplify(s.visit(p))
        ans.append(to_string(p))
    ans.pop()
    return " = ".join(ans)
